-- | Data module holding the server state data type.
module App.State where
import App.Options
import DBus.Client (Client(..))
import EmacsClient
import EmacsDaemon

-- | EmacsDM Server State; the options, a list of daemons, and connected clients
data ServerState = ServerState
                   { options :: Options
                   , dbus :: Maybe Client
                   , daemons :: [ EmacsDaemon ]
                   , clients :: [ EmacsClient ]
                   }
