module Main where
import App.State
import App.Options
import App.Server
import App.Client
import EmacsDaemon
import EmacsDaemon.SessionPredicates
import EmacsClient
import Util
import System.Environment

-- | Main function
main :: IO ()
main = do
  cliOptions <- parseOptions <$> getArgs
  if optServer cliOptions then
    server ServerState { options = cliOptions
                       , daemons = [ ]
                       , clients = [ ]
                       , dbus = Nothing
                       }
  else client cliOptions
