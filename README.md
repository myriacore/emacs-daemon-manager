# Emacs Daemon Manager

**WARNING:** EmacsDM is *very early alpha level software*. Currently, mostly
everything doesn't work!

Emacs Daemon Manager (EmacsDM) is a tool that allows you to run multiple emacs
servers at a time. This allows for, among other things, a
one-window-one-session-like editing experience usually reserved for non-daemon
emacs sessions.

## Why?

I love emacs for its guts-open customizability, but the more elisp you add to
your init.el, the slower emacs is to start. Most people use emacs in daemon mode
to get around this - you can start emacs as quick as you want. However, my
experience with daemon mode had me longing for the convenience of the
traditional one-window-one-session experience. When running on an emacsclient,
the changes you make to (for example) the window layout stick around.

You *could* make it so that closing an emacsclient window closes the emacs
daemon, but then you have to deal with startup times again next time you open
emacs. 

To counteract this, emacs daemon manager always keeps an idle emacs daemon
around in the event that the user wants to start a new session. That way, the 
user can have isolated emacs servers for each of their emacs windows, and still
have the speedup associated with running emacs daemons. 

## Planned Features

- [ ] Server-client operation without the need for more than 1 binary
- [ ] Allows full freedom of commandline arguments to be passed in for either
      the client or the server
- [ ] Working Directory Awareness:
      If an editing session is started under the `~/stuff` directory, and the
      user wants EmacsDM to open the file `~/stuff/morestuff/editme.txt`, 
      EmacsDM will be able to know that it shouldn't open a new emacs frame, and
      should instead use the frame already connected to the `~/stuff` session.
