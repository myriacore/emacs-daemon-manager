{-# LANGUAGE MultiParamTypeClasses #-}
-- | Small utility functions used elsewhere.
module Util where
import Control.Monad.Trans.Maybe

-- | Lifts a 'Maybe' value into a 'MaybeT'.
-- Wraps the value in a polymorphic MaybeT M. This means that IO-returning
-- functions should simply use the @'MaybeT'@ constructor, rather than this. 
maybeT :: Monad m => Maybe a -> MaybeT m a
maybeT = MaybeT . return


-- | Monads that can be embedded into other types of monads are Liftable.
-- Useful for converting between monads in situations where transformers really
-- can't be used. 
class (Monad m, Monad m') => Liftable m m' where
  liftL :: m a -> m' a
  joinL :: m' (m a) -> m' a
  joinL l = l >>= liftL

instance Liftable Maybe IO where
  liftL Nothing = fail "Nothing"
  liftL (Just a) = return a
