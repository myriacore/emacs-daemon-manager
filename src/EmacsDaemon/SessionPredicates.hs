-- | Session Predicates for use with @'newSession'@
module EmacsDaemon.SessionPredicates
where

import EmacsDaemon


-- | Create a new daemon only if there are less than @n@ fresh daemons available
-- Always keeps no less than @n@ fresh daemons idling. If less than that are
-- present when given to @'newSession'@, they will be created until the list
-- has at least @n@ fresh daemons.
keepNFresh
  :: Int             -- ^ Number of idle daemons to keep around @n@
  -> [ EmacsDaemon ] -- ^ List of daemons
  -> Bool            -- ^ Whether or not to create a new session
keepNFresh n = (> n) . length . (filter isFree)
