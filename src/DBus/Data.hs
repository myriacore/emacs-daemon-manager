{-# LANGUAGE OverloadedStrings #-}
-- | Just some important data about the EmacsDM dbus service.
module DBus.Data where
import DBus

-- | Top level DBus object path.
object :: ObjectPath
object = "/org/myriacore/EmacsDM"

-- | Top-level DBus interface name
interface :: InterfaceName
interface = "org.myriacore.EmacsDM"

-- | The Busname for emacs clients
bus :: BusName
bus = "org.myriacore.EmacsDM"
