{-# LANGUAGE OverloadedStrings #-}
module App.Client where
import App.State
import App.Options
import EmacsClient
import DBus
import DBus.Data
import DBus.Client

-- * DBus IPC
-- The client must be able to call the DBus methods that we defined in
-- 'App.Server', so we'll need some code to represent them.
  
-- ** Methods
-- Representations of method calls for the listDaemons, listFreeDaemons, and
-- beginSession dbus functions that we defined in 'App.Server'. These will be
-- called by the client in order to help choose which free client we want to
-- run our session in.

-- | A Dbus 'MethodCall' representation of 'dbusBeginSession`
-- This Dbus method should be called from the client when the client needs to
-- start a new editing session with the server. 
methodBeginSession :: String -> MethodCall
methodBeginSession dName =
  (methodCall object interface "beginSession")
    { methodCallDestination = Just bus
    , methodCallReplyExpected = False
    , methodCallAutoStart = False -- TODO: potentially re-evaluate
    , methodCallBody = [ toVariant dName ]
    }

-- | A Dbus 'MethodCall' representation of 'listDaemons'
-- This dbus method should be called when the client needs a list of daemons
methodListDaemons :: MethodCall
methodListDaemons =
  (methodCall object interface "listDaemons")
    { methodCallDestination = Just bus
    , methodCallReplyExpected = True
    , methodCallAutoStart = False -- TODO: potentially re-evaluate
    }

-- | A Dbus 'MethodCall' representation of 'listFreeDaemons'
-- This dbus method should be called when the client needs a list of free
-- daemons. 
methodListFreeDaemons :: MethodCall
methodListFreeDaemons =
  (methodCall object interface "listFreeDaemons")
    { methodCallDestination = Just bus
    , methodCallReplyExpected = True
    , methodCallAutoStart = False -- TODO: potentially re-evaluate
    }
  
-- * EmacsDM Client
-- The next few functions implement the logic of a client process trying to edit
-- a file. 

client :: Options -> IO ()
client opts = do
  dbus <- connectSession
  -- pick a free daemon to start a session with
  retval <- methodReturnBody <$> call_ dbus methodListFreeDaemons
  -- TODO: deal with explicit case
  case (sequence $ filter ((/=) Nothing) $ map fromVariant retval :: Maybe [String]) of
    Nothing -> fail "Nothing. Fuck you"
    Just freeDaemons -> do
      callNoReply dbus (methodBeginSession (freeDaemons !! 0))
      -- launch emacsclient
      mEmacsClient <- startEmacsClient (freeDaemons !! 0) (optRest opts)
      return ()
