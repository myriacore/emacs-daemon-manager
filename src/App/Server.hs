{-# LANGUAGE OverloadedStrings #-}
module App.Server where
import App.State
import App.Options
import EmacsDaemon
import EmacsDaemon.SessionPredicates
import EmacsClient
import Util
import DBus
import Data.IORef
import Data.List
import DBus.Data
import DBus.Client as D
import Control.Concurrent (threadDelay)
import Control.Monad
import Control.Exception

-- * DBus IPC
-- ** Automethod Functions
-- Following are a few functions that will allow us to expose server state via
-- dbus, and query the server when we need to. This is important because it is
-- the responsibility of the client to chose which free daemon it wants to
-- connect to, so we need to make sure the client has access to this
-- information.

-- | Creates a dbus 'autoMethod' that returns the list of daemon names
listDaemons :: IORef ServerState -> (IO [ String ])
listDaemons st =
  map EmacsDaemon.name <$> daemons <$> readIORef st

-- | Creates a dbus 'autoMethod' that returns the names of all /free/ daemons
listFreeDaemons :: IORef ServerState -> (IO [ String ])
listFreeDaemons st =
  map EmacsDaemon.name <$> filter isFree <$> daemons <$> readIORef st

-- | Creates a dbus 'autoMethod' that allows clients to begin an editing session
-- See documentation in the module 'DBus' about editing sessions. 
dbusBeginSession
  :: IORef ServerState
  -- ^ Mutable Server State. Will be written to if successful
  -> ([EmacsDaemon] -> Bool)
  -- ^ Daemon Creation Predicate. See 'beginSession' for details.
  -> (String -> IO ())
dbusBeginSession st pred daemon = do
  -- TODO: Find a way to clean up the explicit dealings w/ Maybe
  m <- find ((==) daemon . EmacsDaemon.name) <$> daemons <$> readIORef st
  case m of
    Nothing -> fail "Nothing"
    Just d -> do
      Just dbus <- dbus <$> readIORef st
      mdaemons' <- newSession pred dbus =<< daemons <$> readIORef st
      case mdaemons' of
        Nothing -> fail "Nothing"
        Just daemons' -> do
          modifyIORef st (\st' -> st' { daemons = daemons' })
          return ()

-- | Creates a dbus 'autoMethod' that will close an emacs session.
-- This wouldn't probably be exposed to the client - it'd probably be used to
-- remove an emacs daemon from a list once it exits. 
dbusCloseSession
  :: IORef ServerState
  -> ([EmacsDaemon] -> Bool)
  -> (String -> IO ())
dbusCloseSession st pred daemon = do
  daemons' <- filter ((/=) daemon . EmacsDaemon.name) <$> daemons <$> readIORef st
  modifyIORef st (\st' -> st' { daemons = daemons' })

-- | Initializes DBus Server
-- In particular, this exposes important functions that the EmacsDM Client will
-- make use of.
startDBusAsServer
  :: IORef ServerState
  -> ([EmacsDaemon] -> Bool)
  -- ^ Daemon creation predicate. See 'beginSession' for details. 
  -> IO D.Client
startDBusAsServer st pred = do
  dbus <- connectSession
  requestName dbus bus [nameReplaceExisting]
  modifyIORef st (\st' -> st' { dbus = Just dbus })
  export dbus object $
    defaultInterface { interfaceName = interface
                     , interfaceMethods =
                         [ autoMethod "listDaemons" (listDaemons st)
                         , autoMethod "listFreeDaemons" (listFreeDaemons st)
                         , autoMethod "beginSession" (dbusBeginSession st pred)
                         ]
                     }
  return dbus

-- * EmacsDM Server
-- The next few functions make up the main loop of the EmacsDM server. This 
-- involves starting new emacs daemons when needed, removing emacs daemons from
-- the list when the user closes the number of client out, etc.

-- | The server main loop.
-- Waits for dbus messages from the EmacsDM client and makes decisions based on
-- the received messages.
--
-- At the time of writing, this function hardcodes the session predicate, meaing
-- users are not able to control how many free daemons are kept around. Later
-- this might be encoded in the ServerState's 'Options' field.
server :: ServerState -> IO ()
server st = do
  globalState <- newIORef $ st { dbus = Nothing }
  dbus <- startDBusAsServer globalState (keepNFresh 1)
  Just idleDaemon <- getOrCreateFreeDaemon [] dbus 
  modifyIORef globalState (\st' -> st' { daemons = (idleDaemon : daemons st')})
  -- I'm pretty sure dbus methods are called in other threads, so i think we
  -- have to keep this process alive until the user kills it or something.
  (forever $ threadDelay 1) `catch` (closeServer globalState)
  where closeServer st e  = do
          if e /= UserInterrupt then
            print "Something bad happened!"
          else return ()
          Just dbus <- dbus <$> readIORef st
          releaseName dbus bus
          disconnect dbus
