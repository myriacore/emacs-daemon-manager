-- | Module for command line options parsing mechanisms
module App.Options where

-- * Commandline Options parsing
-- There's pretty minimal options parsing, since we're passing so much to the
-- child programs rather than building our own parsing stuff from scratch, but
-- it's important enough that we'll give it its own section.

-- | Super simple options structure that we'll use for the time being
data Options = Options { optServer :: Bool
                       -- ^ Run EmacsDM Server (True), or just client (False)
                       , optRest :: [ String ]
                       -- ^ Other options (to be passed to child process)
                       }

-- | Parse Command line arguments into the options structure
parseOptions
  :: [ String ]
  -> Options
parseOptions ("-s" : args) =
  Options { optServer = True, optRest = args}
parseOptions args =
  Options { optServer = False, optRest = args}
