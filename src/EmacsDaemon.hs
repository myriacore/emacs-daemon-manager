{-# LANGUAGE OverloadedStrings #-}
-- | Facilities for managing running emacs daemons.
module EmacsDaemon
  ( EmacsDaemon(..)
  , getFreeDaemon
  , getOrCreateFreeDaemon
  , newSession
  , beginSession
  ) where
import Util (maybeT)
import System.Process
import Control.Exception
import Control.Concurrent
import DBus
import DBus.Client as D
import DBus.Data
import Data.Maybe
import Data.List
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Maybe
import Control.Applicative ((<|>))

-- * Daemons
-- Emacs Daemons are daemonized emacs processes (e.g. running with @emacs
-- --daemon@).

-- | Represents an Emacs Daemon. 
data EmacsDaemon = EmacsDaemon
                   { name :: String  -- ^ The daemon's @server-name@
                   -- , pid :: Int      -- ^ The PID of the daemon process
                   , isFree :: Bool
                   -- ^ Whether or not the daemon is "free".
                   -- An emacs daemon is considered free if the user hasn't
                   -- started an editing session with it yet. Free daemons are
                   -- always idle, just sitting around waiting for a connection.
                   -- For information on editing sessions, see 'Sessions'.
                   }
  deriving (Show, Eq)

-- | Creates an unused name for new emacs daemons
genFreshName
  :: [ EmacsDaemon ] -- ^ List of current daemons
  -> String          -- ^ Unused name for new daemon
genFreshName eds = "session" ++ (show $ length eds)

-- | Creates a new free emacs daemon.
-- This function spawns a daemonized emacs process, then returns the
-- @'EmacsDaemon'@ representation.
createDaemon
  :: [ EmacsDaemon ]         -- ^ List of current sessions
  -> D.Client                -- ^ DBus Client (for closing the session)
  -> IO (Maybe EmacsDaemon)  -- ^ New EmacsDaemon
createDaemon eds dbus = runMaybeT $ do
  let freshName = genFreshName eds
  liftIO $ forkIO $ do
    pid <- spawnCommand $ "emacs --daemon='" ++ freshName ++ "' >/dev/null"
    waitForProcess pid
    -- Close the session once the daemon has exited
    callNoReply dbus (methodCloseSession freshName)
  return EmacsDaemon { name = freshName
                     , isFree = True
                     }
 
-- | Gets the first free emacs daemon in the list of emacs daemons.
-- Returns @Nothing@ if no free daemons can be found.
getFreeDaemon
  :: [ EmacsDaemon ]   -- ^ List of emacs daemons to search.
  -> Maybe EmacsDaemon -- ^ Free emacs daemon
getFreeDaemon eds = find isFree eds

-- | Finds or creates a free emacs daemon.
-- Searches the given list for a free emacs daemon. Creates a new one if a
-- free emacs daemon cannot be found.
getOrCreateFreeDaemon
  :: [ EmacsDaemon ]         -- ^ List of existing emacs daemons
  -> D.Client                -- ^ DBus Client (for closing new sessions)
  -> IO (Maybe EmacsDaemon)  -- ^ Free emacs daemon
getOrCreateFreeDaemon eds dbus =
  case getFreeDaemon eds of
    Just freeDaemon -> return $ Just freeDaemon
    Nothing -> createDaemon eds dbus

-- * Sessions
-- Sessions are emacs daemons that are currently being used. This is distinct
-- from free daemons, which idle until the user decides to use them. 
  
-- | Consumes a daemon into a session, conditionally creates a new free daemon.
-- Returns the list of new daemons. A predicate is provided determining whether
-- a new daemon should be created.
newSession
  :: ([EmacsDaemon] -> Bool)
  -- ^ Predicate that determines whether a new free daemon should be created.
  -- If it returns true, a new free daemon will be created, and added to the
  -- list. Otherwise, a new free daemon will not be created.
  -- 
  -- For a list of session predicates, see 'EmacsDaemon.SessionPredicates'.
  -> D.Client -- ^ Dbus Client (for closing new sessions)
  -> [ EmacsDaemon ]
  -> IO (Maybe [ EmacsDaemon ])
newSession createMore dbus eds = runMaybeT $
  if createMore eds then do
    free <- MaybeT $ createDaemon eds dbus
    MaybeT $ newSession createMore dbus (free : eds)
  else do
    sessionDaemon <- maybeT $ getFreeDaemon eds
    let eds' = delete sessionDaemon eds
    session <- maybeT $ beginSession sessionDaemon
    return (session : eds')

-- | The user starts an editing session with the given emacs daemon.
-- Returns @Nothing@ if the given emacs daemon wasn't free. 
beginSession
  :: EmacsDaemon       -- ^ Free daemon that will be used for the new session
  -> Maybe EmacsDaemon -- ^ The new editing session
beginSession (ed @ EmacsDaemon {isFree = True}) =
  Just $ ed { isFree = False }
beginSession _ = Nothing

-- | 'MethodCall' for 'dbusCloseSession'.
methodCloseSession :: String -> MethodCall
methodCloseSession daemon =
  (methodCall object interface "closeSession")
    { methodCallDestination = Just bus
    , methodCallReplyExpected = False
    , methodCallAutoStart = False -- TODO: potentially re-evaluate
    , methodCallBody = [ toVariant daemon ]
    }
