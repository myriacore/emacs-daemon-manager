-- | Facilities for setting up editing sessions in an emacs client.
module EmacsClient where
import EmacsDaemon
import Control.Monad.Trans.Maybe
import Control.Monad.IO.Class
import DBus.Client
import System.Process

-- * Emacs Clients
-- Emacs Clients are emacs windows connected to an emacs daemon. As such we'll
-- need some ways of tracking which clients are connected to which daemons, so
-- we can close the emacs clients when the daemons close. 

-- | The datatype we'll use to represent an emacs client. 
data EmacsClient = EmacsClient
                   { name :: String
                   -- ^ The name of the connected Daemon
                   , processHandle :: ProcessHandle
                   -- ^ The processhandle for this emacs Client
                   , pid :: Int
                   -- ^ The PID for this emacsclient's process
                   }

-- | Creates a new emacsclient connected to the given emacs daemon.
startEmacsClient
  :: String     -- ^ Name of @'EmacsDaemon'@ the client will connect to
  -> [ String ] -- ^ Arguments to the emacs client (ignores -s)
  -> IO (Maybe EmacsClient)
startEmacsClient ed args = runMaybeT $ do
  procHandle <- liftIO $ spawnCommand $ "emacsclient -s '" ++ ed ++ "'"
  pid <- MaybeT $ getPid procHandle
  return EmacsClient { EmacsClient.name = ed
                     , EmacsClient.processHandle = procHandle
                     , EmacsClient.pid =
                         (fromInteger . toInteger $ pid :: Int)
                     }

-- According to: https://www.emacswiki.org/emacs/MultiEmacsServer#toc7
-- emacsclient -s {servername} is how we connect to servers
