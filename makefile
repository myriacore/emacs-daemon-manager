all: stack

stack: 
	stack install --local-bin-path .out

clean-stack:
	rm -f -r .stack-work
	rm -f -r .out

clean: clean-stack

doc:
	stack haddock
